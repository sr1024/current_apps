import logging
logger = logging.getLogger(__name__)

import os, sys, traceback, json
import cherrypy
import time, datetime
import splunk.appserver.mrsparkle.controllers as controllers
from splunk.appserver.mrsparkle.lib.decorators import expose_page
from splunk.appserver.mrsparkle.lib.routes import route
import splunk.appserver.mrsparkle.lib.util as util
import splunk.appserver.mrsparkle.lib.i18n as i18n
from mako.template import Template
import splunk.entity as en
import splunk.rest
from splunk.appserver.mrsparkle.lib.capabilities import Capabilities
import StringIO
from zipfile import ZipFile, ZIP_DEFLATED
from string import Template

packager = None

try:
    def log_errors(fn):
        def wrapper(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except Exception, ex:
                logger.error('ERROR: In instrumentation_controller.py: ' + traceback.format_exc())
                raise
        return wrapper

    def instrumentation_endpoint(require_authorization=True):
        def decorator(fn):
            @expose_page(must_login=True)
            @log_errors
            def wrapper(self, *args, **kwargs):
                if require_authorization:
                    check_telemetry_authorization(self, *args, **kwargs)

                import_packager()

                return fn(self, *args, **kwargs)
            return wrapper

        return decorator

    def check_telemetry_authorization(self, *args, **kwargs):
        self.user = en.getEntity('authentication/users', cherrypy.session['user']['name'])
        if 'edit_telemetry_settings' not in self.user.properties['capabilities']:
            self.deny_access()

    def import_packager():
        global packager
        if not packager:
            import splunk_instrumentation.packager as _packager
            packager = _packager

    class InstrumentationController(controllers.BaseController, Capabilities):
        """
        This controller implements custom endpoints used by the
        Splunk Metrics application.
        """

        @instrumentation_endpoint(require_authorization=False)
        @route(methods='GET')
        def instrumentation_eligibility(self, **kwargs):
            '''
            Determines whether the UI for the instrumentation app should be visible,
            including the initial opt-in modal and all settings/logs pages.
            This is determined by user capabilities, license type, and server roles.
            '''
            cherrypy.response.headers['Content-Type'] = 'application/json'

            serverInfo = en.getEntity('server', 'info')

            if util.isCloud():
                return json.dumps({
                    'is_eligible': False,
                    'reason': 'UNSUPPORTED'
                })

            # Must be done before calling `authentication/users`, as that endpoint
            # is not available on the free product.
            if serverInfo.get('isFree', '0') == '1':
                return json.dumps({'is_eligible': True})

            user = en.getEntity('authentication/users', cherrypy.session['user']['name'])

            if 'edit_telemetry_settings' not in user.properties['capabilities']:
                return json.dumps({
                    'is_eligible': False,
                    'reason': 'UNAUTHORIZED'
                })

            if (self.check_server_roles_for_eligibility(serverInfo.properties['server_roles'])):
                return json.dumps({'is_eligible': True})
            else:
                return json.dumps({
                    'is_eligible': False,
                    'reason': 'UNSUPPORTED'
                })

        def check_server_roles_for_eligibility(self, serverRoles):
            '''
            Args:
              - serverRoles: A list of server roles (strings)
              
            Returns:
              True or False, indicating whether this server type is supported
            '''
            
            roles = {}
            for role in serverRoles:
                roles[role] = True

            # The whitelist determines what nodes are even considered
            # for instrumentation eligibility. All nodes that contain
            # any of these server roles will be considered (but may
            # ultimately still be rejected based on the blacklist, etc.)
            whitelist = [
                # Search heads are the typical place to access the UI.
                'search_head',
                # Some search heads lack the search_head role and instead
                # report as shc_member or shc_captain
                'shc_member',
                'shc_captain',
                # Have to whitelist indexer to cover single instance deployments.
                # (A single instance is not a "search head" - search heads only
                #  exist when paired with separate indexers).
                'indexer'
            ]
            
            # The blacklist immediately rejects servers that have any of
            # the blacklisted roles.
            blacklist = [
                # The cluster master does not propagate conf settings to the search
                # heads, so we blacklist it for the UI to avoid inconsistent configurations
                # in the cluster.
                'cluster_master',
                # We've whitelisted indexers to handle the single instance case.
                # However, in a distributed deployment you should only be configuring
                # settings on the SH's (since they will propagate values correctly
                # within the cluster), so we'll blacklist cluster_slaves to catch
                # this case.
                'cluster_slave',
                # Heavyweight forwarder is never considered eligible.
                'heavyweight_forwarder'
            ]
            
            special_case_rejections = [
                # Besides cluster_slaves (handled in the blacklist) you may
                # also have indexers that are part of a distributed deployment
                # but not an indexer cluster (so, not cluster_master or cluster_slave).
                # In that case, we can detect the clustered deployment by
                # checking if the server has become a "search_peer".
                # (Note that we cannot blacklist search_peer entirely, as in
                #  the case of DMC the search heads will be search_peers as well.)
                roles.get('indexer') and roles.get('search_peer')
            ]

            if (any((roles.get(role) for role in whitelist))
                    and not any((roles.get(role) for role in blacklist))
                    and not any(special_case_rejections)):
                return True
            return False

        @instrumentation_endpoint()
        @route(methods='POST')
        def send_anonymous_usage_data(self, **kwargs):
            return self.send_usage('anonymous', **kwargs)

        @instrumentation_endpoint()
        @route(methods='POST')
        def send_license_usage_data(self, **kwargs):
            return self.send_usage('license', **kwargs)

        def send_usage(self, visibility, **kwargs):
            earliest, latest = self.get_earliest_and_latest(**kwargs)
            events = self.get_events_package(earliest, latest, [visibility])
            if events:
                self.send_events_package(events, earliest, latest, [visibility])
                return json.dumps({'sent_count': len(events)})
            return json.dumps({'sent_count': 0})

        @instrumentation_endpoint()
        @route(methods='GET')
        def anonymous_usage_data(self, **kwargs):
            earliest, latest = self.get_earliest_and_latest(**kwargs)
            if self.isMoreThanOneYear(earliest, latest):
                raise cherrypy.HTTPError(code=403, message="Date range must be less than 1 year.")
            zip_file_name, json_file_name = self.get_file_name(earliest, latest, 'Diagnostic')

            cherrypy.response.headers['Content-Disposition'] = ('attachment; filename="%s"' % (zip_file_name))
            cherrypy.response.headers['Content-Type'] = 'application/zip'

            value = self.get_file_value(earliest, latest, ['anonymous'])
            temp_value = self.write_to_string(json_file_name, value)
            if temp_value:
                return temp_value
            raise cherrypy.HTTPError(403)

        @instrumentation_endpoint()
        @route(methods='GET')
        def license_usage_data(self, **kwargs):
            earliest, latest = self.get_earliest_and_latest(**kwargs)
            if self.isMoreThanOneYear(earliest, latest):
                raise cherrypy.HTTPError(code=403, message="Date range must be less than 1 year.")
            zip_file_name, json_file_name = self.get_file_name(earliest, latest, 'License Usage')

            cherrypy.response.headers['Content-Disposition'] = ('attachment; filename=%s' % zip_file_name)
            cherrypy.response.headers['Content-Type'] = 'application/zip'

            value = self.get_file_value(earliest, latest, ['license'])
            temp_value = self.write_to_string(json_file_name, value)
            if temp_value:
                return temp_value
            raise cherrypy.HTTPError(403)


        def get_file_name(self, earliest, latest, data_type, file_type=['zip', 'json']):
            filename = Template('%s Data - %s to %s.$filename' % (
                data_type,
                ('%d.%02d.%02d' % (earliest.year, earliest.month, earliest.day)),
                ('%d.%02d.%02d' % (latest.year, latest.month, latest.day))
            ))
            return [filename.substitute(filename = ft) for ft in file_type]

        def write_to_string(self, json_file_name, value):
            temp = StringIO.StringIO()
            with ZipFile(temp, 'w', ZIP_DEFLATED) as myzip:
                myzip.writestr(json_file_name, value)
            return temp.getvalue()

        def get_file_value(self, earliest, latest, visibility):
            _packager = packager.Packager(splunkrc=self.splunkrc())
            deployment_id = _packager.get_deploymentID()
            transaction_id = _packager.get_transactionID()
            value = self.get_events_package(earliest, latest, visibility, forExport=True)

            ret_value = {
                "deploymentID": deployment_id,
                "transactionID": transaction_id,
                "data": value}
            return json.dumps(ret_value)

        def get_events_package(self, earliest, latest, visibility, forExport=False):
            _packager = packager.Packager(splunkrc=self.splunkrc())
            return _packager.build_package(earliest, latest, visibility, forExport)

        def send_events_package(self, package, earliest, latest, visibility):
            _packager = packager.Packager(splunkrc=self.splunkrc())
            _packager.manual_send_package(package, earliest, latest, visibility)

        def get_earliest_and_latest(self, **kwargs):
            self.assert_earliest_and_latest_provided(**kwargs)
            return self.timestamp_to_internal_repr(kwargs.get('earliest'), kwargs.get('latest'))

        def assert_earliest_and_latest_provided(self, **kwargs):
            if not kwargs.get('earliest') or not kwargs.get('latest'):
                raise Exception("earliest and latest query params are required")

        def timestamp_to_internal_repr(self, *args):
            result = []
            for arg in args:
                result.append(datetime.datetime.fromtimestamp(float(arg)))

            if len(result) == 1:
                return result[0]
            else:
                return result

        def splunkrc(self):
            return {
                'token': cherrypy.session['sessionKey'],
                'server_uri': splunk.rest.makeSplunkdUri()
            }

        def isMoreThanOneYear(self, earliest, latest):
            copyEarliest = earliest.replace(year=earliest.year + 1)
            if latest > copyEarliest:
                return True
            return False

except Exception, ex:
    logger.error('ERROR while loading instrumentation_controller.py: ' + traceback.format_exc())
    raise
