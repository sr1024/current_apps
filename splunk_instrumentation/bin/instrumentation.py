# $SPLUNK_HOME/etc/apps/hello_mi/bin/hello.py

import sys , os
import argparse
from datetime import date
from time import sleep
from string import Template


import xml.dom.minidom, xml.sax.saxutils


args = {}

'''
This but happen before splunk_instrumentation.constants is imported
'''
parser = argparse.ArgumentParser()
parser.add_argument('--scheme' , action='store_true')
parser.add_argument('-v', '--validate-arguments' , action='store_true')
parser.add_argument('--no-collect' , action='store_true',  help='will not collect and index data' )
parser.add_argument('--no-send' ,action='store_true',  help='will not query _telemetry and send data' )
parser.add_argument('-m', '--mode', default="INPUT", help='is required if not running from splund modular inputs')
parser.add_argument( '--collect-today-only', action='store_true', help='will set the collect date to today')
parser.add_argument( '--collect-date', help='will set the collect date ex: 2016-07-22')
parser.add_argument( '--test-schema')
parser.add_argument( '--log-level')
parser.add_argument( '--execution-id')

args = parser.parse_args()

if args.mode:
    os.environ['INST_MODE'] = args.mode
if args.no_collect:
    os.environ['INST_NO_COLLECT'] = args.no_collect
if args.no_send:
    os.environ['INST_NO_SEND'] = args.no_send
if args.test_schema:
    os.environ['INST_TEST_SCHEMA'] = args.test_schema
if args.collect_today_only:
    os.environ['INST_COLLECT_DATE'] = date.today().strftime("%Y-%m-%d")
if args.collect_date:
    os.environ['INST_COLLECT_DATE'] = args.collect_date
if args.log_level:
    os.environ['INST_DEBUG_LEVEL'] = args.log_level
if args.execution_id:
    os.environ['INST_EXECUTION_ID'] = args.execution_id



options = {
    "collect_date": 'INST_COLLECT_DATE',
    'test_schema': 'INST_TEST_SCHEMA',
    "log_level": 'INST_DEBUG_LEVEL',
    "execution_id": 'INST_EXECUTION_ID'
}

description = {
    'INST_COLLECT_DATE': "DESCRIPTION",
    'INST_COLLECT_TODAY_ONLY': "DESCRIPTION",
    'INST_TEST_SCHEMA': 'DESCRIPTION',
    'INST_DEBUG_LEVEL': "DESCRIPTION",
    'INST_EXECUTION_ID': 'DSC'
}

def do_scheme():
    start = """
        <scheme>
            <title>Instrumentation</title>
            <description>Collect instrumentation data across Splunk deployment</description>
            <use_external_validation>true</use_external_validation>
            <streaming_mode>simple</streaming_mode>

            <options>
                <args>
        """

    arg_base = Template("""
                    <arg name="$name">
                        <title>$title</title>
                        <description> $desc
                        </description>
                    </arg>
        """)

    arg = ""

    for name, value in dict(os.environ).iteritems():
        if name in description:
            arg += arg_base.substitute(name=name, title=value, desc=description[name])

    end = """
                </args>
            </options>
        </scheme>
    """

    with open(os.path.dirname(
            os.path.realpath(__file__))+'/scheme.txt', 'w') as target:
        doc = xml.dom.minidom.parseString(start+arg+end)
        target.write(doc.toxml())
    print start + arg + end

    sys.exit(0)

# Routine to get the value of an input
def get_key():
        # read everything from stdin
        config_str = sys.stdin.read()
        with open(os.path.dirname(
                os.path.realpath(__file__)) + '/log.txt', 'w') as target:
            target.write(config_str)
        if config_str.find("session_key") > -1:
            # parse the config XML
            doc = xml.dom.minidom.parseString(config_str)
            root = doc.documentElement


            session_key = root.getElementsByTagName("session_key")[0]
            if session_key and session_key.firstChild and \
                    session_key.firstChild.nodeType == session_key.firstChild.TEXT_NODE:
                session_key_str = session_key.firstChild.data
                os.environ['INST_TOKEN'] = session_key_str

            server_uri = root.getElementsByTagName("server_uri")[0]
            if server_uri and server_uri.firstChild and \
                    server_uri.firstChild.nodeType == server_uri.firstChild.TEXT_NODE:
                server_uri = server_uri.firstChild.data
                os.environ['INST_SERVER_URI'] = server_uri

            # target.write(config_str)
            for node in root.getElementsByTagName("param"):
                option = node.attributes.item(0).value
                value = node.firstChild.data
                if options.get(option):
                    os.environ[options[option]] = value
        else:
            os.environ['INST_TOKEN'] = config_str.rstrip()




if len(sys.argv) > 1:
    if sys.argv[1] == "--scheme":
        do_scheme()

if os.environ['INST_MODE'] == "INPUT":
    get_key()

from splunk_instrumentation.input import run_input
from splunk_instrumentation.constants import  INST_PRE_EXECUTE_SLEEP

# Empty validation routine. This routine is optional.
def validate_arguments():
    pass


# Routine to index data
def run_script():
    sleep(INST_PRE_EXECUTE_SLEEP)
    run_input()


# Script must implement these args: scheme, validate-arguments


if args.scheme:
    do_scheme()
elif args.validate_arguments:
    validate_arguments()
else:
    run_script()

sys.exit(0)
