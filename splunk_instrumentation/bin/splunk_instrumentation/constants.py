import random

# TODO: Should be more like...
# filepath = os.path.join(os.environ['SPLUNK_HOME'], 'etc', 'apps', APP_NAME, 'var(or something)', ".last_read")
LAST_READ_TIME_FILE = '.last_read'
import os , sys, json
import splunk.rest as rest

COLLECTION_NAME = "instrumentation"
INSTRUMENTATION_SOURCETYPE = 'splunk_telemetry'
INSTRUMENTATION_INDEX_NAME = '_telemetry'
AUDIT_INDEX_NAME = '_telemetry'
AUDIT_SOURCETYPE = "splunk_telemetry_log"

INST_PRE_EXECUTE_SLEEP = 60

DEFAULT_QUICKDRAW = {"url" : "https://cdf7b95f.api.splkmobile.com/1.0/cdf7b95f"}
QUICKDRAW_URL = "https://quickdraw.splunk.com/telemetry/destination"

INST_DEBUG_LEVEL = os.environ.get('INST_DEBUG_LEVEL') or "ERROR"

INST_MODE = os.environ.get('INST_MODE') or "INPUT"
# INST_MODE = os.environ.get('INST_MODE') or "PROD"
INST_NO_COLLECT = os.environ.get('INST_NO_COLLECT') or False
INST_NO_SEND = os.environ.get('INST_NO_SEND') or False
INST_COLLECT_DATE = os.environ.get('INST_COLLECT_DATE') or False
INST_SCHEMA_FILE = os.environ.get('INST_SCHEMA_FILE') or os.path.dirname(os.path.realpath(__file__))+'/schema.json'
INST_EXECUTION_ID = os.environ.get('INST_EXECUTION_ID') or "".join(random.choice('0123456789ABCDEF') for i in range(30))

INST_KV_OWNER = "nobody"
INST_KV_APP = "splunk_instrumentation"

INST_PROFILE_OWNER = "nobody"
INST_PROFILE_APP = "splunk_instrumentation"

INST_LICENSE_TYPES = ['anonymous', 'license']

SPLUNKRC = { "token": os.environ.get('INST_TOKEN') or None ,
            "server_uri" : os.environ.get('INST_SERVER_URI') or rest.makeSplunkdUri()
}


VISIBILITY_PATH = {
    "license_usage": "feed.entry.content.sendLicenseUsage",
    "anonymized_usage": "feed.entry.content.sendAnonymizedUsage"
}


KV_STORE_ENDPOINT = {"config": "storage/collections/config",
                        "document": "storage/collections/data/" + COLLECTION_NAME + "/"}

APP_INFO_ENDPOINT = {"info": "telemetry/general","retry" : "telemetry/general/retryEdit"}

path = os.path.realpath(os.path.dirname(os.path.realpath(__file__)) + '/../')
sys.path.append(path)
sys.path.append(os.environ.get('SPLUNK_HOME') + '/etc/apps/framework/contrib/splunk-sdk-python')


if INST_MODE == "DEV":
    if not os.environ.get("SPLUNK_DB"):
        os.environ['SPLUNK_DB'] = os.environ.get('SPLUNK_HOME') + '/var/lib/splunk'
    INST_PRE_EXECUTE_SLEEP = 1
    if not os.environ.get('INST_KEY'):
        rc_file=os.path.dirname(os.path.realpath(__file__))+'/splunkrc.json'
        SPLUNKRC = json.loads(open(rc_file, 'r').read())
