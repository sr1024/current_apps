from data_point import DataPoint
from data_point import registerDataPoint
from datetime import datetime, timedelta, date , time
from splunk_instrumentation.indexing.instrumentation_index import InstrumentationIndex
from splunk_instrumentation.datetime_util import date_to_timestamp_str


class SPLDataPoint(DataPoint):
    def __init__(self,dataPointSchema, options = {}):
        super(SPLDataPoint,self).__init__(dataPointSchema, options)

    def collect(self,dateRange):

        spl = self.sanitizeSPL(self.dataPointSchema.dataPointSchema['spl'],dateRange)
        splunkrc = self.options.get('splunkrc')
        instrumentationIndex = InstrumentationIndex(splunkrc=splunkrc)

        kwargs = {
            "earliest_time" :  dateRange['start'],
            "latest_time" :  dateRange['stop']
        }

        if isinstance(kwargs['earliest_time'],datetime):
            kwargs['earliest_time'] = kwargs['earliest_time'].date()
        if isinstance(kwargs['latest_time'],datetime):
            kwargs['latest_time'] = kwargs['latest_time'].date()

        if isinstance(kwargs['earliest_time'],date):
            kwargs['earliest_time'] =   date_to_timestamp_str(datetime.combine(kwargs['earliest_time'],time.min))
        if isinstance(kwargs['latest_time'],date):
            kwargs['latest_time'] =   date_to_timestamp_str(datetime.combine(kwargs['latest_time'],time.max))

        events = instrumentationIndex.search(spl,**kwargs)

        eventList = []
        for event in events:
            eventList.append(event)
        return eventList

    def sanitizeSPL(self,spl,dateRange):
        spl = spl.replace("%%d", dateRange['start'].strftime('%m-%d-%Y'))
        spl = spl.replace("%%b", dateRange['start'].strftime('%m-%d-%Y'))
        spl = spl.replace("%%e", dateRange['stop'].strftime('%m-%d-%Y'))
        return spl

registerDataPoint(SPLDataPoint)