
from datetime import datetime ,date , timedelta, time as timeObj
import time

def date_to_timestamp_str(dateObj):
    return  "%d" % date_to_timestamp(dateObj)

def date_to_timestamp(dateObj):
    if (isinstance(dateObj, datetime)):
        return int(time.mktime(dateObj.timetuple()))
    if (isinstance(dateObj, date)):
        return int(time.mktime(dateObj.timetuple()))
    if (isinstance(dateObj, float)):
        return int(float(dateObj))
    if (isinstance(dateObj, int)):
        return int(dateObj)
    if (isinstance(dateObj, timeObj)):
        return int(str(time))
    if (isinstance(dateObj, timeObj)):
        return int(dateObj)
    return 0