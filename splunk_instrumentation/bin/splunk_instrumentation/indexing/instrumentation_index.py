import logging

logger = logging.getLogger(__name__)

import json, sys, os

from base_class import BaseClass
from splunk_instrumentation.constants import *
from datetime import date

RANGE_TYPE_TIMESTAMP = 1
RANGE_TYPE_DATE = 2


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, date):
        serial = obj.isoformat()
        return serial
    raise TypeError("Type not serializable")


class InstrumentationIndex(BaseClass):
    def __init__(self, splunkrc=None, index_name=INSTRUMENTATION_INDEX_NAME):
        super(InstrumentationIndex, self).__init__(splunkrc=splunkrc)
        self._set_index(index_name)

    # Public API
    # ----------

    def process_new_events(self, start, end, callback, range_type=RANGE_TYPE_TIMESTAMP, visibility=[]):
        '''
        Calls `callback` with an iterable of new events.
        If callback does not throw an exception, the events will no
        longer be "new."
        '''
        if range_type == RANGE_TYPE_TIMESTAMP:
            events = self._query_by_timestamp(start, end, visibility)
        if range_type == RANGE_TYPE_DATE:
            events = self._query_by_date(start, end, visibility)

        results = []
        for event in events:
            results.append(json.loads(event.get('_raw')))
        callback(results)

    def submit_json(self, event, host=None, source=None):
        '''
        Submit a new json event directly to the index.
        If the event is not a string already, it will be converted with `json.dumps`.
        '''
        if not isinstance(event, str):
            event = json.dumps(event, default=json_serial)
        self._index.submit(event, host=host, source=source, sourcetype=INSTRUMENTATION_SOURCETYPE)

    # "Private" methods
    # -----------------
    def _query_by_timestamp(self, t_start, t_end, visibility):
        search_cmd = 'search index=' + self.index_name
        search_cmd += " sourcetype=" + INSTRUMENTATION_SOURCETYPE
        if t_start:
            search_cmd += (' earliest=%s' % t_start.strftime("%m/%d/%Y:%H:%M:%S"))
        if t_end:
            search_cmd += (' latest=%s' % t_end.strftime("%m/%d/%Y:%H:%M:%S"))

        visibility_cmd = self._get_visibility_cmd(visibility)
        search_cmd += " (%s)" % visibility_cmd

        return self._query(search_cmd)

    def _query_by_date(self, t_start, t_end, visibility):
        search_cmd = 'search index=' + self.index_name
        search_cmd += " sourcetype=" + INSTRUMENTATION_SOURCETYPE + " | spath date | search "
        if t_start:
            search_cmd += (' date>=%s' % t_start.strftime("%Y-%m-%d"))
        if t_end:
            search_cmd += (' date<=%s' % t_end.strftime("%Y-%m-%d"))

        visibility_cmd = self._get_visibility_cmd(visibility)
        search_cmd += " (%s)" % visibility_cmd

        return self._query(search_cmd)

    def _get_visibility_cmd(self, visibility):
        if not visibility:
            visibility = INST_LICENSE_TYPES

        return " OR ".join(["visibility= *" + str(x) + "*" for x in visibility])
