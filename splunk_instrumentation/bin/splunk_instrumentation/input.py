'''
This is the main entry point to scripted inputs to run
'''
from constants import *
import sys
import json
import logging
from report import report
from time import sleep
from schedule_manager import ScheduleManager
from splunk_instrumentation.dataPoints.data_point import dataPointFactory
from splunk_instrumentation.metrics.metrics_schema import MetricsSchema, load_schema
from splunk_instrumentation.dataPoints.spl_data_point import SPLDataPoint
from splunk_instrumentation.metrics.instance_profile import get_instance_profile

logging.root
logging.root.setLevel(INST_DEBUG_LEVEL)
formatter = logging.Formatter('%(levelname)s %(message)s')
handler = logging.StreamHandler(stream=sys.stderr)
handler.setFormatter(formatter)
logging.root.addHandler(handler)

report.report('executionID', INST_EXECUTION_ID)


def run(schema_file):
    ms = load_schema(schema_file)
    sm = ScheduleManager(ms, dataPointFactory)
    logging.info("INST Started")
    profile = get_instance_profile()
    if profile.visibility['anonymized_usage']:
        sm.collect()
    if not INST_NO_COLLECT and not INST_NO_SEND:
        sleep(5)
    if not INST_NO_SEND:
        sm.send()
    logging.info("INST Done")


def eval_instance(profile, requirements, result, label):
    for reqi in requirements:
        if (reqi[0] == "!"):
            reqi = reqi.replace("!", "")
            if profile.roles.get(reqi):
                return None
        elif not profile.roles.get(reqi):
            return None
    report.report("instance.type", label)
    return result


def can_run():
    '''
    This list is eveluated in order
    '''
    req_list = [
        {
            "requirements": ['indexer', '!search_peer', '!cluster_slave', '!shc_member'],
            "label": "Single",
            "result": True
        },

        {
            "requirements": ['cluster_master'],
            "label": "Cluster Master",
            "result": True
        },
        {
            "requirements": ['!cluster_master', 'in_cluster'],
            "label": "Cluster Member not Cluster Master",
            "result": False
        },

        ## assume we are already not a cluster member from the above requirements
        {
            "requirements": ['shc_captain'],
            "label": "Search Captain in a non cluster",
            "result": True
        },
        {
            "requirements": ['!cluster_master', 'search_head', '!search_peer', '!in_cluster', '!cluster_slave', '!shc_member'],
            "label": "Single Search Head",
            "result": True
        },
    ]
    profile = get_instance_profile()
    if not (profile.visibility['license_usage'] or profile.visibility['anonymized_usage']):
        report.report("not-opted-in", True)
        return False

    report.report("profile.visibility", profile.visibility)
    report.report("profile.cluster_mode", profile.profile.get('cluster_mode'))
    report.report("profile.roles", profile.roles)

    if (profile.roles.get('search_head') and not profile.roles.get('shc_member')) or profile.roles.get('sh_captain'):
        report.report("profile.retry_transaction", True)
        profile.retry_transaction()

    for req in req_list:
        result = eval_instance(profile, req['requirements'], req['result'],
                               req['label'])
        if (not result == None):
            return result

    if profile.server_info.get('product_type') == "splunk":
        report.report("instance.type", 'Cloud')
        return False

    report.report("instance.type", 'No Match')
    return False


def run_input():
    if can_run():
        run(INST_SCHEMA_FILE)

    report.send()
