"""InstanceProfile class."""

from splunklib import data as spldata
import splunk.entity as en
import xml.dom.minidom as dom
from urllib import urlencode
from splunk_instrumentation.constants import *
from splunk_instrumentation.indexing.base_class import BaseClass


class InstanceProfile(BaseClass):
    """InstanceProfile.

    This class will retrive the instance's information.

    self.server_info = server information will be stored here
    self.visibility  = visibility information will be stored here
    """

    def __init__(self, splunkrc=SPLUNKRC):
        """Constructor."""
        splunkrc = (splunkrc or SPLUNKRC).copy()
        splunkrc['owner'] = splunkrc.get('owner') or INST_PROFILE_OWNER
        splunkrc['app'] = splunkrc.get('app') or INST_PROFILE_APP
        super(InstanceProfile, self).__init__(splunkrc)
        self.profile = {}
        self.service = self._splunkd.service
        self.headers = [('Accept', 'application/json')]
        self.server_info = dict(en.getEntity('server', 'info'))
        self.roles = {role: True for role in self.server_info['server_roles']}

        # gets cluster info from endpoint
        self._load_json({"end_point": "cluster/config/config", "name": "cluster_config"})

        # if call fails set cluster_mode to disabled
        self.profile['cluster_mode'] = self._nested_get(self.profile, 'cluster_config.entry.content.mode', 'disabled')

        # gets search captian info from endpoint. noProxy is required so that it fails when instance is not the captain
        self._load_json({"end_point": "shcluster/captain/info", "name": "captain_info"}, noProxy=True, default={})

        # if captain/info returns a value it is caption  : overwrites server roles
        # this is failing so removing for the time being
        # self.roles['shc_captain'] = bool(self.profile.get('captain_info'))

        # if mode is not disabled then add in_cluster to roles   : overwrites server roles
        self.roles['in_cluster'] = not self.profile.get('cluster_mode') == 'disabled'
        #   overwrites server roles
        self.roles['cluster_master'] = self.profile.get('cluster_mode') == 'master'

        self._get_visibility()

    def _get_visibility(self):
        self.visibility = {}
        payload = self.service.request(
            APP_INFO_ENDPOINT["info"], method="GET", headers=self.headers,
            owner=self.splunkrc['owner'], app=self.splunkrc['app']).get('body')

        if payload:
            payload = (spldata.load(payload.read()))
            for key, value in VISIBILITY_PATH.iteritems():
                self.visibility[key] = int(self._nested_get(payload, value))
            self.retryTransaction = self._nested_get(payload, 'feed.entry.content.retryTransaction')

    def _nested_get(self, dic, path, default=0):
        """NestedGet.

        default value is 0
        """
        keys = path.split(".")
        for key in keys[:-1]:
            dic = dic.setdefault(key, {})

        if type(dic) is dict:
            return default
        return dic.get(keys[-1])

    def retry_transaction(self):
        if self.retryTransaction:
            queryObj = {}
            for key, value in VISIBILITY_PATH.iteritems():
                queryObj[value.split(".")[-1]] = self.visibility[key]

            query = urlencode(queryObj)
            retryTransactions = self.retryTransaction.split(",")
            for retryTransaction in retryTransactions:
                query += '&' + urlencode({"retryTransaction": retryTransaction})
            try:
                response = self.service.request(APP_INFO_ENDPOINT["retry"], method="POST",
                                                body=query, owner=self.splunkrc['owner'],
                                                app=self.splunkrc['app'])
            except Exception as e:
                return False
            return True

    def _load_json(self, endpoint, noProxy=False, default={}):
        '''
        calls endpoint['end_point'] and assigns the results to `self.profile[end_point['name']]`
        :param endpoint:
        :return:
        '''
        try:
            path = self.service.authority \
                   + self.service._abspath(endpoint["end_point"], owner=self.splunkrc['owner'],
                                           app=self.splunkrc['app'])
            all_headers = self.service._auth_headers
            if (noProxy):
                path += "?noProxy=true"
            payload = self.service.http.request(path,
                                                {'method': 'GET',
                                                 'headers': all_headers}).get('body')
            if payload:
                result = (spldata.load(payload.read()))
                self.profile[endpoint['name']] = result['feed']
        # often if lisence does not permit this call it will return a 402 as exception
        except Exception as e:
            self.profile[endpoint['name']] = default
            return False

        return True


def get_instance_profile(splunkrc=None):
    get_instance_profile.instance = get_instance_profile.instance or InstanceProfile(splunkrc)
    return get_instance_profile.instance


get_instance_profile.instance = None
