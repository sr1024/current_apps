import datetime
import time
import json
import logging
from datetime import date, datetime, timedelta

from metrics_transforms import transform_object
from splunk_instrumentation.constants import *
from splunk_instrumentation.datetime_util import date_to_timestamp
from splunk_instrumentation.report import report

class MetricsCollectionManager:

    def __init__(self, metricSchema , dataPointFactory, splunkrc=None):
        self.metricSchema = metricSchema
        self.dataPointFactory = dataPointFactory
        self.splunkrc = splunkrc

    def collect_data(self, eventList, on_send=False):
        classes = self.metricSchema.getEventClassByfield(on_send, "on_send",False)

        for classDef in classes:
            if (INST_COLLECT_DATE):
                nextDate = datetime.strptime(INST_COLLECT_DATE, "%Y-%m-%d").date()
            else:
                nextDate = date.today()-timedelta(days=1)

            self.collect_class_data(eventList, classDef, nextDate)

    def collect_data_date_range(self, eventList, start, stop, on_send=False):
        classes = self.metricSchema.getEventClassByfield(on_send, "on_send")
        dateRange = {"start":start,"stop":stop}

        for classDef in classes:
                self.collect_class_data(eventList, classDef, dateRange)


    def collect_class_data(self, eventList, classDef, dateRange):
        try:
            if not isinstance(dateRange,dict):
                dateRange =  { "start" : dateRange}
            dateRange["stop"]  = dateRange.get("stop")  or dateRange.get("start")
            dataPoints = classDef.getDataPoints()
            classResults = {}


            for dataPoint in dataPoints:
                dataPointResult = self.collect_data_point(dataPoint,dateRange)
                report.report('collected[]',
                              {"component": classDef.component, "dateRange": dateRange, "count": len(dataPointResult)})

                if isinstance(dataPointResult,dict):
                    self.data_point_results_transform(classDef, dataPointResult, classResults,dateRange)
                else:
                    logging.info("count " + str(len(dataPointResult)))
                    for event in dataPointResult:
                        event = self.data_point_results_transform( classDef, event, {} ,dateRange)
                        eventList.append(event)

            if len(classResults.keys()):
                eventList.append(classResults)
        except  Exception as e:
            report.report('exceptions[]',str(e))


    def collect_data_point(self, dataPoint, dateRange):
        dataPointObj = self.dataPointFactory(dataPoint,options={"splunkrc":self.splunkrc})
        data = dataPointObj.collect(dateRange)
        return data

    def data_point_results_transform(self, classDef, dataPointResult, classResults,dateRange):
        fields = classDef.index_fields

        result = {"data":None}
        if (dataPointResult['data']):
            data = json.loads(dataPointResult['data'])
            data = transform_object(data=data,fields=fields)
            result['data'] = data
        result['timestamp'] = date_to_timestamp(datetime.now())

        result['component'] = classDef.component
        result['date'] = dateRange['stop'].isoformat()
        try:
            if 'T' in dataPointResult.get('_time'):
                result['date'] = dataPointResult.get('_time').split('T')[0]
        except:
            result['date'] = dateRange['stop'].isoformat()

        result['visibility'] = classDef.visibility or "anonymous"
        result['executionID'] = INST_EXECUTION_ID

        return result
