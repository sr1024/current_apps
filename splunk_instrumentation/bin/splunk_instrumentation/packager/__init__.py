from splunk_instrumentation.indexing.instrumentation_index import InstrumentationIndex , RANGE_TYPE_TIMESTAMP , RANGE_TYPE_DATE
from send_log import SendLog
from send_data import SendData
import random
import logging
from datetime import datetime, timedelta
from splunk_instrumentation.indexing.kv_store import KvStore
from splunk_instrumentation.indexing.license_data import LicenseData
from splunk_instrumentation.metrics.instance_profile import get_instance_profile
from splunk_instrumentation.metrics.metrics_schema import MetricsSchema ,load_schema
from splunk_instrumentation.constants import *
from splunk_instrumentation.metrics.metrics_collection_manager import MetricsCollectionManager
from quick_draw import get_quick_draw
from splunk_instrumentation.report import report
from splunk_instrumentation.metrics.metrics_transforms import transform_object
from splunk_instrumentation.dataPoints.data_point import dataPointFactory
from splunk_instrumentation.dataPoints.spl_data_point import SPLDataPoint
from splunklib import binding

logger = logging.getLogger(__name__)

class Packager(object):

    def __init__(self,
            splunkrc=None, deploymentID=None, schema=None, factory=None):
        self._splunkrc = splunkrc
        self.deploymentID = deploymentID
        self.transaction_id = None
        if not schema:
            schema = load_schema(INST_SCHEMA_FILE)
        self.schema = schema
        if not factory:
            factory = dataPointFactory
        self.factory = factory

        self.deliverySchema = self.schema.delivery
        qd = get_quick_draw()
        if qd:
            self.deliverySchema.url = qd.get('url')
        self.transaction_id = self.get_transactionID()
        self.sd = SendData(
            deploymentID=self.get_deploymentID(),
            deliverySchema=self.deliverySchema,
            transaction_id=self.get_transactionID())
        self.sl = SendLog(splunkrc=self._splunkrc)

    def package_send(self):
        """Auto send and log data.

        First we look at our index and check the start, stop, and visibility
        Next we query based on that, and send it.
        """
        start, stop, visibility = self.get_info_for_log()
        if start is False:
            return False

        events = self.query_events(
            start, stop, RANGE_TYPE_TIMESTAMP, visibility)

        if len(events) == 0:
            report.report('send-canceled', True)
            return False
        return self.send_package(events, start, stop)

    def get_info_for_log(self):
        '''
        Returns a tuple (start, stop, visibility) describing the parameters for the
        next pending auto phone-home job. Start & stop are timestamps correspoding
        to the sourcetype=splunk_telemetry events "_time" (not "date") field.
        '''
        i = get_instance_profile(self._splunkrc)
        if not i.visibility['license_usage'] and not i.visibility['anonymized_usage']:
            logging.info('has not opt in')
            return (False, 0, [])

        stop = datetime.now()
        start = datetime.now() - timedelta(hours=6)

        report.report("package.send_dates", [start, stop])

        # The expected behavior for sends is a bit odd: When opted in for anonymous
        # data sharing but not license, we still send license data (which is hashed as anonymous).
        # So in all cases license data is sent.

        visibility = []
        if i.visibility['license_usage'] or i.visibility['anonymized_usage']:
            visibility.append("license")
        if i.visibility['anonymized_usage']:
            visibility.append("anonymous")
        return start, stop, visibility

    def get_transactionID(self):
        if self.transaction_id:
            return self.transaction_id
        allowedCharacters = '0123456789ABCDEF'
        transaction_id = ''.join(random.choice(allowedCharacters) for i in range(8)) + '-' + ''.join(
            random.choice(allowedCharacters) for i in range(4)) + \
               '-' + ''.join(random.choice(allowedCharacters) for i in range(4)) + \
               '-' + ''.join(random.choice(allowedCharacters) for i in range(4)) + \
               '-' + ''.join(random.choice(allowedCharacters) for i in range(12))
        self.transaction_id = transaction_id
        return self.transaction_id

    def get_deploymentID(self):
        kv = KvStore(splunkrc=self._splunkrc)
        deploymentID = self.deploymentID or kv.get_key(
            'instrumentation_deploymentID')
        if not deploymentID:
            deploymentID = self._createGUID()
            logging.info("createGUID" + str(deploymentID))
            kv.set_key('instrumentation_deploymentID', deploymentID)

        self.deploymentID = deploymentID
        return deploymentID

    def get_visibility(self,events):
        result = {}
        i = get_instance_profile(self._splunkrc)
        visibility = {}
        visibility['anonymous'] = i.visibility.get('anonymized_usage')
        visibility['license'] = i.visibility.get('license_usage')

        for event in events:
            vis = event.get('visibility') or []
            for key in vis.split(','):
                if visibility.get(key):
                    result[key] = True
        return sorted(result.keys())

    def _createGUID(self):
        allowedCharacters = '0123456789ABCDEF'
        return "".join(random.choice(allowedCharacters) for i in range(30))

    def build_package(self, start, stop, visiblity, forExport=False):
        return self.query_events(
                start, stop, RANGE_TYPE_DATE, visiblity, forExport)

    def get_license_data(
            self, events, start, stop, range_type=RANGE_TYPE_TIMESTAMP):
        if (range_type == RANGE_TYPE_TIMESTAMP):
            if (start.date() == stop.date()):
                return []
            stop = stop - timedelta(days=1)
        mcm = MetricsCollectionManager(
            self.schema, self.factory, splunkrc=self._splunkrc)
        mcm.collect_data_date_range(events, start.date(), stop.date(), True)

    def query_events(
            self, start, stop, range_type=RANGE_TYPE_TIMESTAMP,
            visibility=[], forExport=False):
        # can hard code
        i = InstrumentationIndex(splunkrc=self._splunkrc)
        result = []
        self.get_license_data(result, start, stop, range_type)

        def process_events(events):
            for data in events:
                self._transform_data(data)
                result.append(data)
        i.process_new_events(
            start, stop, process_events,
            range_type=range_type, visibility=visibility)
        if forExport:
            result = self.mark_visibility(result, visibility, 'manual')

        return result

    def _transform_data(self, data):
        classDef = self.schema.getEventClassByfield(data['component'])
        if (len(classDef)):
            data['data'] = transform_object(data['data'], classDef[0].fields)

        return data

    def send_package(
            self, events, start, stop, method='auto', visibility=None):
        """Sending package and log it.

        If offline (or quickdraw not available), log failed to the index.
        events = events from index
        start = from datetime picker
        stop = from datetime picker
        method = ['auto', 'manual']
        visibility = [anonymous, license]
        """

        visibility = visibility or self.get_visibility(events)
        self.sl.send_attempted(start, stop, visibility, method)
        try:
            if self.deliverySchema.url:
                events = self.mark_visibility(events, visibility, method)
                self.sd.send_data(events)
                self.sl.send_completed(start, stop, visibility, method)
            else:
                raise Exception('Quickdraw is not available')
        except binding.HTTPError as e:
            logger.error(e)
            self.sl.send_failed(start, stop, visibility, method)
            report.report("send_failed", True)
        except Exception as e:
            logger.error(e)
            self.sl.send_failed(start, stop, visibility, method)
            report.report("send_failed", True)
        except:
            logger.error("Unknown Error")
            self.sl.send_failed(start, stop, visibility, method)
            report.report("send_failed", True)

    def manual_send_package(self, events, start, stop, visibility):
        """Handling manually sending package from the UI.

        This is just a wrapper for send_package
        events = events from index
        start = from datetime picker
        stop = from datetime picker
        visibility = [anonymous, license]
        """
        return self.send_package(
            events, start, stop, method='manual', visibility=visibility)

    def mark_visibility(self, events, visibility, method='auto'):
        """Marking visibility.

        This only runs when user manually export or send data.
        It alters the visibility field according to their choice from the UI
        events = events from index
        visibility = [anonymous, license] from UI
        """
        if method == 'manual':
            for event in events:
                event['visibility'] = ','.join(visibility)
        elif method == 'auto':
            for event in events:
                temp = []
                for vis in event['visibility'].split(','):
                    if vis in visibility:
                        temp.append(vis)
                event['visibility'] = ','.join(temp)
        else:
            raise Exception("Should never reach this.")

        return events
