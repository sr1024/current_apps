import json
import requests
from splunk_instrumentation.constants import *



def get_quick_draw():
    if get_quick_draw.quick_draw_results:
        return get_quick_draw.quick_draw_results
    url =  QUICKDRAW_URL
    try:
        response = requests.get(url)
        response = json.loads(response.text)
        get_quick_draw.quick_draw_results = response
    except:
        return None
    return response

get_quick_draw.quick_draw_results = None