import json
import time
from datetime import date, datetime
import random
import requests
from splunk_instrumentation.report import report


CDS_API_VERSION = "1"
def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, date):
        serial = obj.isoformat()
        return serial
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial

    raise TypeError ("Type not serializable")

class SendData:

    def __init__(self, endpoint = None, authKey = None,
            deploymentID = None, deliverySchema = None, transaction_id=None):
        self.deploymentID = deploymentID
        self.deliverySchema = deliverySchema
        self.source = None
        self.transaction_id = transaction_id

    def send_data(self, data):
        time.sleep(6)

        n = 10
        groups = [data[i:i + n] for i in range(0, len(data), n)]

        for group in groups:
            self.send_events(group)

        report.report("SendDatalength",len(data))

    def send_events(self,data):
        headers = {"Content-type": "text/json"}

        payload = self.bundle_DTOs(data)
        url = "/".join([self.deliverySchema.url, self.deploymentID,str(len(data)),"0"]) + "?hash=none"
        response = requests.post(url, data=payload, headers=headers)

    def bundle_DTOs(self,dtos):
        timestamp = str(int(time.time()))

        def convert(data):
            data['transactionID'] = self.transaction_id
            data['deploymentID'] = self.deploymentID
            data['version'] = self.deliverySchema.version

            separator = '^'.join(["{", CDS_API_VERSION, "event", timestamp ])+"}"
            result = {"sdkVersion": "4.3", "osVersion": "0", "event_name": "Deployment", "appVersionCode": "3", \
                      "uuid": self.deploymentID, "packageName": "splunk_instrumentation", "extraData": data, \
                      "session_id": self.transaction_id, "appVersionName": "1"}
            return json.dumps(result,default=json_serial) + separator

        return "".join( map(convert, dtos))
