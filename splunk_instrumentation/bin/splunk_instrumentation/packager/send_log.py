"""SendLog class."""

from datetime import datetime
import time
import logging
import json
from splunk_instrumentation.constants import AUDIT_INDEX_NAME, INST_EXECUTION_ID , AUDIT_SOURCETYPE
from splunk_instrumentation.indexing.base_class import BaseClass
from splunk_instrumentation.datetime_util import date_to_timestamp_str


METHOD = {
    "AUTO": "auto",
    "MANUAL": "manual"
}

class SendLog(BaseClass):
    """SendLog class."""

    def __init__(
        self, splunkrc=None, index_name=AUDIT_INDEX_NAME,
            inst_key=None, inst_host=None, inst_port=None):
        """
        Constructor.

        This class inherits from BaseClass.
        """
        super(SendLog, self).__init__(splunkrc=splunkrc)
        self.splunkrc['source'] = "telemetry"
        self.index_name = index_name
        self._set_index(self.index_name)
        self._status = {
            "ATTEMPTED": 'attempted',
            "SUCCESS": 'success',
            "FAILED": 'failed'
        }

    def send_attempted(self, start, end, visibility, method=METHOD['AUTO']):
        """send_attempted.

        Send status attempted into index_name
        start = a datetime Object
        end = a datetime Object
        visibility = ['licese', 'anonymous']
        method = ['auto', 'manual']
        """
        logging.info(
            "attempt send "+date_to_timestamp_str(start)+' to '+date_to_timestamp_str(end))
        self._submit_status(
            self._status["ATTEMPTED"], start, end, visibility,
            source=self.splunkrc['source'], method=method)

    def send_completed(self, start, end, visibility, method=METHOD['AUTO']):
        """send_completed.

        Send status completed into index_name
        start = a datetime Object
        end = a datetime Object
        visibility = ['licese', 'anonymous']
        method = ['auto', 'manual']
        """
        logging.info(
            "completed send "+date_to_timestamp_str(start)+' to '+date_to_timestamp_str(end))
        self._submit_status(
            self._status["SUCCESS"], start, end, visibility,
            source=self.splunkrc['source'], method=method)

    def send_failed(self, start, end, visibility, method=METHOD['AUTO']):
        """send_failed.

        Send status failed into index_name
        start = a datetime Object
        end = a datetime Object
        visibility = ['licese', 'anonymous']
        method = ['auto', 'manual']
        """
        logging.info(
            "failed send "+date_to_timestamp_str(start)+' to '+date_to_timestamp_str(end))
        self._submit_status(
            self._status["FAILED"], start, end, visibility,
            source=self.splunkrc['source'], method=method)

    def get_last_auto_send_log(self):
        """Get the last event recorded to index_name with method = auto """
        search_cmd = 'search index = ' + self.index_name + ' sourcetype='+AUDIT_SOURCETYPE+' method = auto| head 1'
        query_results = [value for value in self._query(search_cmd)]

        if not query_results or len(query_results) == 0:
            return None
        result = json.loads(query_results[0]['_raw'])
        result['start'] = datetime.fromtimestamp(float(result['start']))
        result['end'] = datetime.fromtimestamp(float(result['end']))
        logging.info(
            "get_last_auto_send_log " + date_to_timestamp_str(result['start']) +
            ' to ' + date_to_timestamp_str(result['end']))
        return result

    def _submit_status(
            self, status, start, end, visibility, source=None, method=METHOD['AUTO']):
        if type(visibility) == list:
            visibility = ','.join([str(value) for value in visibility])

        data = {"time": int(time.time()),
                "status": status,
                "start": self._to_time_stamp(start),
                "end": self._to_time_stamp(end),
                "executionID": INST_EXECUTION_ID,
                "visibility": visibility,
                "method": method}

        data = json.dumps(data)
        self.submit_json(
            data, source=self.splunkrc['source'])

    def submit_json(self, event, host=None, source=None):
        """
        Submit a new json event directly to the index.

        If the event is not a string already, it will be converted with
        `json.dumps`.
        """
        if not isinstance(event, str):
            event = json.dumps(event)
        self._index.submit(event, host=host, source=source, sourcetype = AUDIT_SOURCETYPE)

    def _to_time_stamp(self, dt, epoch=datetime(1970, 1, 1)):
        """Convert datetime to unix timestamp."""
        return time.mktime(dt.timetuple())
