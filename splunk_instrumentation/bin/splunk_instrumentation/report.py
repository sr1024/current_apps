import json
from constants import *
from datetime import date , datetime
from datetime_util import date_to_timestamp

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, date):
        serial = obj.isoformat()
        return serial
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial

    raise TypeError ("Type not serializable")


class Report(object):
    def __init__(self):
        self.log = {}

    def report(self,name,value):
        arrayTest = name.split("[")
        name = arrayTest[0]
        arrayTest = len(arrayTest) == 2

        def nested_set( dic, path, value):
            keys = path.split(".")
            for key in keys[:-1]:
                dic = dic.setdefault(key, {})

            if arrayTest:
                dic.setdefault(keys[-1],[])
                dic[keys[-1]].append(value)
            else:
                dic[keys[-1]] = value

        nested_set(self.log,name, value)
        if not INST_MODE == "INPUT":
            print "report::" + name +'='+ json.dumps(value,default=json_serial )

    def send(self):
        self.log.setdefault('timestamp', date_to_timestamp(datetime.now()))
        print json.dumps(self.log,default=json_serial)
    def write(self):
        self.log.setdefault('timestamp', date_to_timestamp(datetime.now()))
        with open(os.path.dirname(
                os.path.realpath(__file__)) + '/report.json', 'w') as target:
            target.write(json.dumps(self.log,default=json_serial))

report = Report()