from splunk_instrumentation.indexing.instrumentation_index import  InstrumentationIndex
from splunk_instrumentation.metrics.metrics_collection_manager import MetricsCollectionManager
from splunk_instrumentation.packager import Packager
import logging
import time



class ScheduleManager(object):
    def __init__(self, schema, factory):
        self.schema = schema
        self.factory = factory

    def collect(self):
        intervals = ['nightly']
        events = []
        for interval in intervals:
            events += self._run_interval(interval)

        time.sleep(2)
        self._send_events(events)

    def send(self):
        p = Packager(schema=self.schema,factory=self.factory)
        p.package_send()


    def _get_interval_from_time(self,time):
        '''
        this code is temp
        :param time:
        :return:
        '''

        return ['hourly']


    def _run_interval(self, interval):
        mcm = MetricsCollectionManager(self.schema, self.factory)
        events = []
        mcm.collect_data(events, False)
        return events

    def _send_events(self, events):
        i = InstrumentationIndex()
        logging.info("events to index: "+str(len(events)))
        for event in events:
            i.submit_json(event)
