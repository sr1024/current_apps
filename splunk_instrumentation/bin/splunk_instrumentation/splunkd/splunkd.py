import splunklib.client
from splunklib.client import Service
from urlparse import urlsplit


class Splunkd(object):
    '''
    A decorator for a splunkd service object, providing
    convenience methods implemented specifically for this
    application.
    '''

    def __init__(self, **kwargs):
        if kwargs.get('token'):
            if kwargs.get('server_uri'):
                splunkd = urlsplit(kwargs.get('server_uri'), allow_fragments=False)
                kwargs['scheme'] = splunkd.scheme
                kwargs['host'] = splunkd.hostname
                kwargs['port'] = splunkd.port
            self.service = Service(**kwargs)
        else:
            self.service = splunklib.client.connect(**kwargs)

    def has_index(self, name):
        return None != self.get_index(name)

    # The normal splunkd.indexes[NAME] method of retrieving
    # an index raises an exception when it does not exist.
    # While a normal dictionary has a `.get` method that simply
    # returns None for DNE, in splunklib this method is used to perform
    # a GET request.
    def get_index(self, name):
        for index in self.service.indexes:
            if index.name == name:
                return index
        return None

    def __getattr__(self, name):
        return getattr(self.service, name)
